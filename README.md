# ChainReact

Modern C++ remake of an old game. Work in progress.

![Start screen](media/start.png)

![Ongoing game](media/ongoinggame.png)

## Build and run

```shell-session
$ meson build
$ meson compile -C build
$ ./build/chainreact-sdl
```

## Dependency

- fontconfig
- SDL2
- SDL_ttf
- [basic_widgets](https://git.nekomata.one/git/sdl2_basic_widgets) (meson will download it for you)
