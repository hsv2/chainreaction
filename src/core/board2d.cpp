#include <iostream>
#include <queue>

#include "board2d.hpp"

#include "player.hpp"

using namespace core;
using namespace std;

Board2D::Board2D(const size_type w, const size_type h)
{
    init(w, h);
}

void Board2D::add_unit(const Coord c)
{
    auto& space = at(c);
    cout << "+ add_unit " << space.coord.x << "," << space.coord.y << " ";
    if (!(space.capacity >= space.value)) throw InvalidMove {"Space is overloaded"};

    space.value++;
    units[space.owner]++;
    cout << "total: " << units[space.owner] << endl;
}

auto Board2D::begin() const noexcept -> const_iterator
{
    return spaces.cbegin();
}

auto Board2D::count(Player player) const noexcept -> float
{
    const float total = [this]() {
        auto count {0};
        for (const auto [_, u] : units) {
            count += u;
        }
        return count;
    }();

    return units.at(player) / total;
}

void Board2D::capture(const Coord c, const Player p) noexcept
{
    auto& s = at(c);
    if (s.owner != p) {
        cout << "= P" << static_cast<int>(p) << " captures " << s.coord.x << ","
             << s.coord.y << " : +" << s.value << endl;
        units[s.owner] -= s.value;
        units[p] += s.value;
        s.owner = p;
    }
}

auto Board2D::end() const noexcept -> const_iterator
{
    return spaces.cend();
}

void Board2D::init(const size_type w_, const size_type h_)
{
    w = w_;
    h = h_;

    spaces.reserve(w_ * h_); // NOLINT
    for (size_type y = 0; y < h_; y++) {
        for (size_type x = 0; x < w_; x++) {
            spaces.push_back(Coord {x, y});
            if (x == 0 || x == w_ - 1) {
                if (y == 0 || y == h_ - 1) {
                    spaces.back().capacity = 1;
                }
                else {
                    spaces.back().capacity = 2;
                }
            }
            else {
                if (y == 0 || y == h_ - 1) {
                    spaces.back().capacity = 2;
                }
                else {
                    spaces.back().capacity = 3;
                }
            }
        }
    }

    units.emplace(Player::P1, 0);
    units.emplace(Player::P2, 0);
}

auto Board2D::is_overloaded(Coord c) const noexcept -> bool
{
    const auto& s = at(c);
    return s.value > s.capacity;
}

auto Board2D::is_valid_move(Coord coord, Player player) const noexcept -> bool
{
    const auto& s = at(coord);
    return s.owner == Player::NONE || s.owner == player;
}

auto Board2D::move(const Coord c, const Player p)
  -> Generator<pair<Space, optional<Player>>>
{
    const auto& space = at(c);
    if (space.owner == Player::NONE) capture(c, p);
    else if (space.owner != p) throw InvalidMove {"Player is not the Space owner"};

    add_unit(c);
    co_yield make_pair(space, nullopt);

    queue<Coord> overloaded;
    if (is_overloaded(c)) {
        overloaded.push(c);
        while (!overloaded.empty()) {
            const auto src     = overloaded.front();
            const auto targets = neighbours(src);
            overloaded.pop();

            auto spreader = spread(src, targets, p);
            while (spreader) {
                const auto& s = spreader();
                co_yield make_pair(s, winner());
            }

            for (const auto t : targets) {
                if (is_overloaded(t)) {
                    cout << "* overloaded: " << t.x << "," << t.y << endl;
                    overloaded.push(t);
                };
            }
        }
    }
}

auto Board2D::neighbours(const Coord c) const noexcept -> vector<Coord>
{
    vector<Coord> nghbrs;

    if (c.x > 0) nghbrs.emplace_back(Coord {c.x - 1, c.y});
    if (c.x < w - 1) nghbrs.emplace_back(Coord {c.x + 1, c.y});
    if (c.y > 0) nghbrs.emplace_back(Coord {c.x, c.y - 1});
    if (c.y < h - 1) nghbrs.emplace_back(Coord {c.x, c.y + 1});

    return nghbrs;
}

auto Board2D::operator()(const Coord c) const noexcept -> const Board2D::Space&
{
    return spaces.at(coord2idx(c));
}

void Board2D::remove_unit(const Coord c)
{
    auto& s = at(c);
    cout << "- remove " << c.x << "," << c.y << " "
         << "total: " << units[s.owner] << endl;
    if (s.value > 1) {
        s.value--;
        units[s.owner]--;
    }
    else if (s.value == 1) {
        s.reset();
        units[s.owner]--;
    }
    else throw logic_error("Space is empty");
}

auto Board2D::size() const noexcept -> pair<size_type, size_type>
{
    return {w, h};
}

auto Board2D::spread(const Coord src, const vector<Coord>& targets, const Player player)
  -> Generator<Space>
{
    const auto& s = at(src);
    cout << "= spread " << src.x << "," << src.y << " " << s.value << "/" << s.capacity
         << endl;
    {
        if (s.value != static_cast<int>(targets.size())) {
            throw logic_error("cannot spread: unit count and target "
                              "spaces count differ");
        }
    }

    for (const auto& t : targets) {
        remove_unit(src);
        co_yield at(src);

        capture(t, player);
        add_unit(t);
        co_yield at(t);
    }
}

auto Board2D::valid_moves(Player player) const noexcept -> vector<Coord>
{
    vector<Coord> moves;
    for (const auto& s : *this) {
        if (is_valid_move(s.coord, player)) moves.push_back(s.coord);
    }

    return moves;
}

auto Board2D::winner() const noexcept -> optional<Player>
{
    Player winner {Player::NONE};

    for (auto [key, value] : units) {
        if (value > 0) {
            if (winner == Player::NONE) winner = key;
            else return nullopt;
        }
    }

    return winner == Player::NONE ? nullopt : make_optional(winner);
}

auto Board2D::at(const Coord c) noexcept -> Space&
{
    return spaces.at(coord2idx(c));
}

auto Board2D::at(const Coord c) const noexcept -> const Space&
{
    return spaces.at(coord2idx(c));
}

auto Board2D::coord2idx(const Coord c) const noexcept -> size_type
{
    return c.y * w + c.x;
}
