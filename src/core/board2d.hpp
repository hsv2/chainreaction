#ifndef CORE_BOARD2D_HPP
#define CORE_BOARD2D_HPP

#include <map>
#include <optional>
#include <stdexcept>
#include <utility>
#include <vector>

#include "generator.hpp"
#include "player.hpp"

namespace core
{
    class Board2D
    {
    public:
        struct Space;
        using Spaces         = std::vector<Space>;
        using const_iterator = Spaces::const_iterator;
        using iterator       = Spaces::iterator;
        using size_type      = Spaces::size_type;

        struct Coord
        {
            size_type x;
            size_type y;
        };

        struct Space
        {
            int    capacity {};
            Coord  coord {};
            Player owner {Player::NONE};
            int    value {0};

            Space() = default;
            Space(Coord c) : coord {c} {}

            inline void reset() noexcept
            {
                owner = Player::NONE;
                value = 0;
            }
        };

        struct InvalidMove : public std::logic_error
        {
            using logic_error::logic_error;
        };

        // Construct a board without initialising it.
        Board2D() = default;
        // Construct and init a board of a given size.
        Board2D(size_type width, size_type height);

        [[nodiscard]] auto begin() const noexcept -> const_iterator;
        [[nodiscard]] auto count(Player) const noexcept -> float;
        [[nodiscard]] auto end() const noexcept -> const_iterator;
        // Initialise the board with given size.
        void               init(size_type, size_type);
        [[nodiscard]] auto is_overloaded(Coord) const noexcept -> bool;
        [[nodiscard]] auto is_valid_move(Coord, Player) const noexcept -> bool;
        // Make a move step by step.
        [[nodiscard]] auto move(Coord, Player)
          -> Generator<std::pair<Space, std::optional<Player>>>;
        // Get coordinate of neighbour spaces.
        [[nodiscard]] auto neighbours(Coord) const noexcept -> std::vector<Coord>;
        // Get space by its coordinate.
        [[nodiscard]] auto operator()(Coord) const noexcept -> const Space&;
        // Get board size as a <width, height> pair.
        [[nodiscard]] auto size() const noexcept -> std::pair<size_type, size_type>;
        [[nodiscard]] auto valid_moves(Player) const noexcept -> std::vector<Coord>;
        // Get the winner, if any.
        [[nodiscard]] auto winner() const noexcept -> std::optional<Player>;

    private:
        size_type             h;
        Spaces                spaces;
        std::map<Player, int> units;
        size_type             w;

        void               add_unit(Coord);
        [[nodiscard]] auto at(Coord) noexcept -> Space&;
        [[nodiscard]] auto at(Coord) const noexcept -> const Space&;
        void               capture(Coord, Player) noexcept;
        [[nodiscard]] auto coord2idx(Coord) const noexcept -> size_type;
        void               remove_unit(Coord);
        auto spread(Coord, const std::vector<Coord>&, Player) -> Generator<Space>;
    };
}

#endif
