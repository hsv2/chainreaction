#ifndef CORE_AI
#define CORE_AI

#include "board2d.hpp"
#include "player.hpp"

namespace core
{
    auto negamax(const Board2D&, Player, int) -> Board2D::Coord;
}

#endif
