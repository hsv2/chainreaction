#include <algorithm>

#include "ai.hpp"

using namespace core;

auto negamax_recurs(const Board2D& board, Player player, int depth, int color) -> float
{
    if (depth == 0 || board.winner()) {
        return color * board.count(player);
    }

    auto value {-1.F};
    for (const auto& coord : board.valid_moves(player)) {
        auto next_board {board};
        for (auto mover = next_board.move(coord, player); mover; mover())
            ;
        auto next_player {player == Player::P1 ? Player::P2 : Player::P1};
        value =
          std::max(value, -negamax_recurs(next_board, next_player, depth - 1, -color));
    }

    return value;
}

auto core::negamax(const Board2D& board, Player player, int depth) -> Board2D::Coord
{
    Board2D::Coord best_move;
    auto           score {-1.F};
    for (const auto& coord : board.valid_moves(player)) {
        auto next_board {board};
        for (auto mover = next_board.move(coord, player); mover; mover())
            ;
        auto next_player {player == Player::P1 ? Player::P2 : Player::P1};
        if (const auto next_score =
              -negamax_recurs(next_board, next_player, depth - 1, -1);
            next_score > score)
        {
            best_move = coord;
            score     = next_score;
        }
    }

    return best_move;
}
