#ifndef CORE_PLAYER_HPP
#define CORE_PLAYER_HPP

namespace core
{
    enum struct Player
    {
        NONE,
        P1,
        P2
    };
}

#endif
