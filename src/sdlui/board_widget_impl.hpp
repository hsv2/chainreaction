#ifndef GUI_BOARD_WIDGET_IMPL_HPP
#define GUI_BOARD_WIDGET_IMPL_HPP

#include <map>
#include <memory>

#include <basic_widgets/core/texture.hpp>
#include <basic_widgets/core/type/color.hpp>
#include <basic_widgets/w/base/widget_impl.hpp>
#include <basic_widgets/w/feat/mouse_handler_impl.hpp>

#include "board_widget.hpp"

namespace sdlui
{
    class BoardWidgetImpl : public virtual BoardWidget,
                            public virtual bwidgets::WidgetImpl,
                            public virtual bwidgets::MouseHandlerImpl
    {
    public:
        BoardWidgetImpl(bwidgets::Widget* parent = nullptr);
        [[nodiscard]] auto board() -> const std::weak_ptr<core::Board2D>& override;
        void               board(std::weak_ptr<core::Board2D>) override;
        [[nodiscard]] auto color_p1() const -> const bwidgets::Color& override;
        void               color_p1(bwidgets::Color) override;
        [[nodiscard]] auto color_p2() const -> const bwidgets::Color& override;
        void               color_p2(bwidgets::Color) override;
        [[nodiscard]] auto default_color_p1() const -> const bwidgets::Color& override;
        [[nodiscard]] auto default_color_p2() const -> const bwidgets::Color& override;
        [[nodiscard]] auto event_type() const -> std::uint32_t override;
        [[nodiscard]] auto size() const noexcept -> bwidgets::Size override;

    private:
        enum class TextureKey
        {
            P1_ATOM,
            P2_ATOM
        };

        [[nodiscard]] auto atom_size() -> int;
        [[nodiscard]] auto coord(SDL_Point) -> core::Board2D::Coord;
        void               discard_textures();
        void               draw_atoms(const SDL_Rect&, core::Player, int);
        auto render_texture(TextureKey) -> const std::shared_ptr<bwidgets::Texture>&;
        [[nodiscard]] auto space(core::Board2D::Coord) -> SDL_Rect;
        [[nodiscard]] auto space_size() const -> bwidgets::Size;

        void _handle_geometry_change(const SDL_Rect&) override;
        void _handle_rendering() override;

        static const bwidgets::Color _default_color_p1;
        static const bwidgets::Color _default_color_p2;

        std::weak_ptr<core::Board2D> _board_wptr;
        const std::uint32_t          _event_type {SDL_RegisterEvents(1)};
        std::map<TextureKey, std::shared_ptr<bwidgets::Texture>> _textures;
        std::map<TextureKey, bwidgets::Color>                    _texture_colors {
          {TextureKey::P1_ATOM, default_color_p1()},
          {TextureKey::P2_ATOM, default_color_p2()}
        };
        SDL_Rect _widget_area;
    };
}

#endif
