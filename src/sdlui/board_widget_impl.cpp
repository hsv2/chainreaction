#include <cstdint>
#include <iostream>

#include <basic_widgets/core/draw.hpp>
#include <basic_widgets/core/math.hpp>

#include "board_widget_impl.hpp"

using namespace bwidgets;
using namespace core;
using namespace sdlui;
using namespace std;

BoardWidgetImpl::BoardWidgetImpl(Widget* parent) : WidgetImpl(parent)
{
    click_handler([this](const SDL_MouseButtonEvent& ev) {
        auto*     c = new Board2D::Coord(coord({ev.x, ev.y}));
        SDL_Event event;
        SDL_zero(event);
        event.type       = event_type();
        event.user.code  = static_cast<uint32_t>(EventCode::MOVE);
        event.user.data1 = c;
        SDL_PushEvent(&event);
    });
    enable_mouse_handler(_widget_area, viewport());
}

auto BoardWidgetImpl::board() -> const weak_ptr<Board2D>&
{
    return _board_wptr;
}

void BoardWidgetImpl::board(weak_ptr<Board2D> b)
{
    _board_wptr = b;
    discard_textures();
    SDL_Event event;
    SDL_zero(event);
    event.type      = event_type();
    event.user.code = static_cast<uint32_t>(EventCode::BOARD_CHANGED);
    SDL_PushEvent(&event);
}

auto BoardWidgetImpl::color_p1() const -> const Color&
{
    return _texture_colors.at(TextureKey::P1_ATOM);
}

void BoardWidgetImpl::color_p1(Color c)
{
    _texture_colors[TextureKey::P1_ATOM] = c;
}

auto BoardWidgetImpl::color_p2() const -> const Color&
{
    return _texture_colors.at(TextureKey::P2_ATOM);
}

void BoardWidgetImpl::color_p2(Color c)
{
    _texture_colors[TextureKey::P2_ATOM] = c;
}

auto BoardWidgetImpl::default_color_p1() const -> const Color&
{
    return _default_color_p1;
}

auto BoardWidgetImpl::default_color_p2() const -> const Color&
{
    return _default_color_p2;
}

auto BoardWidgetImpl::event_type() const -> std::uint32_t
{
    return _event_type;
}

auto BoardWidgetImpl::size() const noexcept -> Size
{
    if (const auto& board = _board_wptr.lock(); !_board_wptr.expired()) {
        return {static_cast<int>(64 * board->size().first),
                static_cast<int>(64 * board->size().second)};
    }

    return {0, 0};
}

auto BoardWidgetImpl::atom_size() -> int
{
    const auto [w, h]   = space_size() * (1 / 4.);
    const auto diameter = w > h ? h : w;
    return diameter;
}

auto BoardWidgetImpl::coord(SDL_Point p) -> Board2D::Coord
{
    const auto [w, h] = space_size();
    if (w < 1 || h < 1) return {0, 0};
    return {static_cast<Board2D::size_type>((p.x - viewport().x - _widget_area.x) / w),
            static_cast<Board2D::size_type>((p.y - viewport().y - _widget_area.y) / h)};
}

void BoardWidgetImpl::discard_textures()
{
    for (const auto k : {TextureKey::P1_ATOM, TextureKey::P2_ATOM}) {
        if (_textures.contains(k)) _textures[k].reset();
    }
}

void BoardWidgetImpl::draw_atoms(const SDL_Rect& draw_area, Player p, int atoms)
{
    const auto texture = p == core::Player::P1 ? render_texture(TextureKey::P1_ATOM)
                                               : render_texture(TextureKey::P2_ATOM);
    if (!texture) return;

    const auto size = atom_size();
    const auto draw = [this, draw_area, size, &texture](int a, int b) {
        const auto x = draw_area.x + (1 + a) * draw_area.w / 4 - size / 2;
        const auto y = draw_area.y + (1 + b) * draw_area.h / 4 - size / 2;
        renderer()->copy(*texture, nullptr, {x, y, size, size});
    };

    switch (atoms) {
        case 0:
            break;
        case 1:
            draw(1, 1);
            break;
        case 2:
            draw(2, 0);
            draw(0, 2);
            break;
        case 3:
            draw(0, 0);
            draw(1, 1);
            draw(2, 2);
            break;
        case 4:
            draw(0, 0);
            draw(0, 2);
            draw(2, 0);
            draw(2, 2);
            break;
    }
}

auto BoardWidgetImpl::render_texture(TextureKey key) -> const shared_ptr<Texture>&
{
    if (!_textures.contains(key)) {
        _textures.emplace(key,
                          filled_circle(_texture_colors[key], atom_size(), *renderer()));
    }
    else if (!_textures[key]) {
        _textures[key] = filled_circle(_texture_colors[key], atom_size(), *renderer());
    }

    return _textures[key];
}

auto BoardWidgetImpl::space(Board2D::Coord c) -> SDL_Rect
{
    const auto [w, h] = space_size();
    return {_widget_area.x + static_cast<int>(c.x) * w,
            _widget_area.y + static_cast<int>(c.y) * h, w, h};
}

auto BoardWidgetImpl::space_size() const -> Size
{
    if (const auto& board = _board_wptr.lock(); !_board_wptr.expired()) {
        const auto [w, h] = board->size();
        return {static_cast<int>(_widget_area.w / w),
                static_cast<int>(_widget_area.h / h)};
    }

    return {0, 0};
}

void BoardWidgetImpl::_handle_geometry_change(const SDL_Rect& vp)
{
    _widget_area = {0, 0, vp.w, vp.h};
    if (vp.w != viewport().w || vp.h != viewport().h) discard_textures();
}

void BoardWidgetImpl::_handle_rendering()
{
    if (const auto board = _board_wptr.lock(); !_board_wptr.expired()) {
        for (const auto& s : *board) {
            const auto space_area = space(s.coord);
            fill_rect_gradient(*renderer(), space_area, theme()->color_widget_border(),
                               theme()->color_widget_bg());
            draw_atoms(space_area, s.owner, s.value);
        }
    }
}

const Color BoardWidgetImpl::_default_color_p1 {52, 101, 164, SDL_ALPHA_OPAQUE};
const Color BoardWidgetImpl::_default_color_p2 {204, 0, 0, SDL_ALPHA_OPAQUE};
