#ifndef GUI_GAME_SCREEN_HPP
#define GUI_GAME_SCREEN_HPP

#include <cstdint>

#include <basic_widgets/w/base/widget.hpp>

namespace core
{
    class Board2D;
}

namespace sdlui
{
    class GameScreen : public virtual bwidgets::Widget
    {
    public:
        [[nodiscard]] virtual auto board() const
          -> const std::shared_ptr<core::Board2D>&                           = 0;
        virtual void               board(std::shared_ptr<core::Board2D>)     = 0;
        [[nodiscard]] virtual auto board_event_type() const -> std::uint32_t = 0;
        virtual void               flash(const std::string&)                 = 0;
        [[nodiscard]] virtual auto header_text() const -> std::string_view   = 0;
        virtual void               header_text(const std::string&)           = 0;
    };
}

#endif
