#ifndef GUI_GAME_SCREEN_IMPL_HPP
#define GUI_GAME_SCREEN_IMPL_HPP

#include <basic_widgets/w/base/widget_impl.hpp>
#include <basic_widgets/w/caption.hpp>

#include "basic_widgets/w/base/layout.hpp"
#include "board_widget.hpp"
#include "game_screen.hpp"

namespace sdlui
{
    class GameScreenImpl final : public virtual GameScreen,
                                 public virtual bwidgets::WidgetImpl
    {
    public:
        GameScreenImpl(bwidgets::Widget* p = nullptr);

        [[nodiscard]] auto board() const
          -> const std::shared_ptr<core::Board2D>& override;
        void               board(std::shared_ptr<core::Board2D>) override;
        [[nodiscard]] auto board_event_type() const -> std::uint32_t override;
        void               flash(const std::string&) override;
        void               handle_event(const SDL_Event&) override;
        [[nodiscard]] auto header_text() const -> std::string_view override;
        void               header_text(const std::string&) override;
        [[nodiscard]] auto size() const noexcept -> bwidgets::Size override;

    private:
        void _handle_geometry_change(const SDL_Rect&) override;
        void
        _handle_renderer_change(const std::shared_ptr<bwidgets::Renderer>&) override;
        void _handle_rendering() override;
        void _handle_theme_change(const std::shared_ptr<bwidgets::Theme>&) override;

        std::shared_ptr<core::Board2D>      _board;
        std::shared_ptr<sdlui::BoardWidget> _board_widget;
        std::shared_ptr<bwidgets::Caption>  _caption_flash;
        std::shared_ptr<bwidgets::Caption>  _caption_height;
        std::shared_ptr<bwidgets::Caption>  _caption_width;
        std::shared_ptr<bwidgets::Caption>  _header;
        std::shared_ptr<bwidgets::Layout>   _menu;
        bool                                _menu_open;
        long long                           _time_flash;
    };
}

#endif
