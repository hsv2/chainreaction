#include <chrono>
#include <cstdint>
#include <iostream>
#include <limits>

#include "game_screen_impl.hpp"
#include <basic_widgets/core/draw.hpp>
#include <basic_widgets/core/math.hpp>
#include <basic_widgets/w/widget_factory.hpp>

#include "board_widget_impl.hpp"

using namespace bwidgets;
using namespace core;
using namespace sdlui;
using namespace std;

GameScreenImpl::GameScreenImpl(Widget* parent)
  : WidgetImpl {parent},
    _board_widget {make_shared<BoardWidgetImpl>(this)},
    _caption_flash {create_caption(this)},
    _caption_height {create_caption(this)},
    _caption_width {create_caption(this)},
    _header {create_caption(this)},
    _menu {create_vertical_layout(this)},
    _menu_open {true},
    _time_flash {0LL}
{
    _caption_flash->alignment(Caption::Alignment::CENTER);
    _caption_flash->render_mode(Font::RenderMode::BLENDED);

    // menu widgets
    auto button_cancel  = create_button();
    auto button_new     = create_button();
    auto input_height   = create_input_int();
    auto input_width    = create_input_int();
    auto layout_buttons = create_horizontal_layout(this);
    auto layout_height  = create_horizontal_layout(this);
    auto layout_width   = create_horizontal_layout(this);

    button_cancel->click_handler([this](auto) { _menu_open = false; });
    button_cancel->text("Cancel");
    button_new->click_handler([this, input_width, input_height](auto) {
        board(make_shared<Board2D>(input_width->value, input_height->value));
        _menu_open = false;
    });
    button_new->text("New");
    _caption_height->text("Height:");
    _caption_width->text("Width:");
    input_height->value_range(2, 8);
    input_width->value_range(2, 8);

    layout_buttons->add_widget(button_cancel);
    layout_buttons->add_widget(button_new);
    layout_height->add_widget(_caption_height);
    layout_height->add_widget(input_height);
    layout_width->add_widget(_caption_width);
    layout_width->add_widget(input_width);

    _menu->add_widget(layout_width);
    _menu->add_widget(layout_height);
    _menu->add_widget(layout_buttons);

    // Toggle menu with Esc.
    _add_event_handler({SDL_KEYUP, [this](const SDL_Event& ev) {
                            if (ev.key.keysym.sym == SDLK_ESCAPE)
                                _menu_open = !_menu_open;
                        }});
}

auto GameScreenImpl::board() const -> const shared_ptr<Board2D>&
{
    return _board;
}

void GameScreenImpl::board(std::shared_ptr<Board2D> board)
{
    _board_widget->board(board);
    header_text("Player 1");
    _board = board;
}

auto GameScreenImpl::board_event_type() const -> uint32_t
{
    return _board_widget->event_type();
}

void GameScreenImpl::flash(const string& s)
{
    _caption_flash->text(s);
    _time_flash = chrono::duration_cast<chrono::milliseconds>(
                    chrono::steady_clock::now().time_since_epoch())
                    .count();
}

void GameScreenImpl::handle_event(const SDL_Event& ev)
{
    WidgetImpl::handle_event(ev);
    if (!_menu_open) _board_widget->handle_event(ev);
    else _menu->handle_event(ev);
}

auto GameScreenImpl::header_text() const -> string_view
{
    return _header->text();
}

void GameScreenImpl::header_text(const string& s)
{
    _header->text(s);
}

auto GameScreenImpl::size() const noexcept -> Size
{
    const auto hdr_size   = _header->size();
    const auto board_size = _board_widget->size();
    return {board_size.w, board_size.h + hdr_size.h};
}

void GameScreenImpl::_handle_geometry_change(const SDL_Rect& vp)
{
    const auto hdr_height {theme()->font_default_biggest()->height};
    _header->viewport({vp.x, vp.y, vp.w, hdr_height});
    _board_widget->viewport({vp.x, vp.y + hdr_height, vp.w, vp.h - hdr_height});
    const auto menu_size {_menu->size()};
    _menu->viewport({vp.x + center_line(vp.w, menu_size.w),
                     vp.y + center_line(vp.h, menu_size.h), menu_size.w, menu_size.h});
    const auto flash_size {_caption_flash->size()};
    _caption_flash->viewport(
      {vp.x, vp.y + center_line(vp.h, flash_size.h), vp.w, flash_size.h});
}

void GameScreenImpl::_handle_renderer_change(const shared_ptr<Renderer>& r)
{
    _board_widget->renderer(r);
    _caption_flash->renderer(r);
    _header->renderer(r);
    _menu->renderer(r);
}

void GameScreenImpl::_handle_rendering()
{
    _board_widget->render();
    _header->render();
    if (_menu_open) {
        renderer()->viewport(&viewport());
        renderer()->draw_color({0, 0, 0, 175});
        renderer()->fill_rect({0, 0, viewport().w, viewport().h});
        renderer()->viewport(&_menu->viewport());
        fill_rect_gradient(*renderer(),
                           {0, 0, renderer()->viewport().w, renderer()->viewport().h},
                           theme()->color_widget_border(), theme()->color_widget_bg());
        _menu->render();
    }
    if (const auto now = chrono::duration_cast<chrono::milliseconds>(
                           chrono::steady_clock::now().time_since_epoch())
                           .count();
        now - _time_flash <= 3000)
    {
        const auto factor {smoothstep(now - _time_flash, 0LL, 3000LL)};
        const auto [x, y, w, h] = _caption_flash->viewport();

        renderer()
          ->viewport(&viewport())
          ->draw_color(lerp({255, 255, 255, 255, true}, {255, 255, 255, 0, true}, factor,
                            true, false))
          ->fill_rect({0, y - viewport().y, w, h});
        _caption_flash->font_color_fg(
          lerp({0, 0, 0, 255, true}, {0, 0, 0, 0, true}, factor, true, false));
        _caption_flash->render();
    }
}

void GameScreenImpl::_handle_theme_change(const shared_ptr<Theme>& t)
{
    _board_widget->theme(t);
    _header->theme(t);
    _header->alignment(bwidgets::Caption::Alignment::CENTER);
    _header->theme(t);
    _header->font(t->font_default_biggest());
    _header->font_color_bg(t->color_widget_bg());
    _header->font_color_fg(t->color_font_fg());
    _header->render_mode(bwidgets::Font::RenderMode::SHADED);
    _menu->theme(t);
    _caption_flash->font(std::make_shared<Font>(Font::find("Serif"), 64));
    _caption_height->font(t->font_default());
    _caption_height->font_color_bg(t->color_widget_bg());
    _caption_height->font_color_fg(t->color_font_fg());
    _caption_height->render_mode(Font::RenderMode::SHADED);
    _caption_width->font(t->font_default());
    _caption_width->font_color_bg(t->color_widget_bg());
    _caption_width->font_color_fg(t->color_font_fg());
}
