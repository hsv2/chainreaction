#include <chrono>
#include <memory>
#include <stdexcept>
#include <thread>

#include <basic_widgets/w/default_theme.hpp>
#include <basic_widgets/w/widget_factory.hpp>

#include "game_screen_impl.hpp"
#include "sdlui.hpp"

using namespace bwidgets;
using namespace core;
using namespace std;
using namespace sdlui;

ChainReactSDL::ChainReactSDL()
  : _accept_input {true},
    _current_player {Player::P1},
    _header_event {SDL_RegisterEvents(1)},
    _move_thread {thread()}
{
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    _window = unique_ptr<SDL_Window, Deleter>(
      SDL_CreateWindow("ChainReact", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640,
                       480, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE));
    _game_screen = make_unique<GameScreenImpl>();
    _renderer    = make_shared<Renderer>(_window.get(), -1, SDL_RENDERER_ACCELERATED);
    _theme       = make_shared<bwidgets::DefaultTheme>();

    _renderer->blend_mode(SDL_BLENDMODE_BLEND);
    _game_screen->renderer(_renderer);
    _game_screen->theme(_theme);
}

ChainReactSDL::~ChainReactSDL()
{
    _game_screen.reset();
    _theme.reset();
    _renderer.reset();
    _window.reset();
    TTF_Quit();
    SDL_Quit();
}

void ChainReactSDL::handle_board_event(const SDL_UserEvent& event)
{
    switch (static_cast<BoardWidget::EventCode>(event.code)) {
        case BoardWidget::EventCode::MOVE: {
            if (!_accept_input) break;
            if (_move_thread.joinable()) _move_thread.join();

            _accept_input         = false;
            const auto* coord     = static_cast<const Board2D::Coord*>(event.data1);
            const auto  move_task = [this](Board2D::Coord coord) {
                try {
                    for (auto mover =
                           _game_screen->board()->move(coord, _current_player);
                         mover;) {
                        const auto [_, winner] = mover();
                        if (winner) {
                            if (winner.value() == core::Player::P1)
                                _game_screen->flash("Player 1 wins!");
                            else _game_screen->flash("Player 2 wins!");
                            return;
                        }
                        this_thread::sleep_for(750ms);
                    }
                    SDL_Event header_update;
                    SDL_zero(header_update);
                    header_update.type = _header_event;
                    if (_current_player == core::Player::P1) {
                        header_update.user.data1 = (void*)"Player 2";
                        _current_player          = core::Player::P2;
                        _game_screen->flash("Player 2 turn…");
                    }
                    else {
                        header_update.user.data1 = (void*)"Player 1";
                        _current_player          = core::Player::P1;
                        _game_screen->flash("Player 1 turn…");
                    }
                    SDL_PushEvent(&header_update);
                    _accept_input = true;
                } catch (const core::Board2D::InvalidMove&) {
                    _game_screen->flash("Invalid move!");
                    _accept_input = true;
                }
            };
            _move_thread = thread(move_task, *coord);
            delete coord;
            break;
        }
        case BoardWidget::EventCode::BOARD_CHANGED:
            if (!_game_screen->board()->winner()) {
                _current_player = Player::P1;
                _accept_input   = true;
            }
            break;
    }
}

void ChainReactSDL::handle_event(const SDL_Event& event)
{
    switch (event.type) {
        case SDL_QUIT:
            _quit = true;
            break;
        case SDL_WINDOWEVENT:
            if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                const auto [w, h] = _renderer->output_size();
                _game_screen->viewport({0, 0, w, h});
            }
            break;
    }
    if (event.type == _game_screen->board_event_type()) {
        handle_board_event(event.user);
    }
    else if (event.type == _header_event) {
        _game_screen->header_text(static_cast<const char*>(event.user.data1));
    }

    _game_screen->handle_event(event);
}

void ChainReactSDL::run()
{
    _quit = false;
    auto player {Player::P1};
    while (!_quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event) != 0) {
            handle_event(event);
        }

        _game_screen->render();
        _renderer->present();
    }
}

int main()
{
    ChainReactSDL app;
    app.run();
}
