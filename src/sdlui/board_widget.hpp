#ifndef GUI_BOARD_WIDGET_HPP
#define GUI_BOARD_WIDGET_HPP

#include <cstdint>

#include <basic_widgets/w/base/widget.hpp>
#include <basic_widgets/w/feat/mouse_handler.hpp>

#include "../core/board2d.hpp"

namespace sdlui
{
    class BoardWidget : public virtual bwidgets::Widget,
                        public virtual bwidgets::MouseHandler
    {
    public:
        using Widget::Widget;

        enum class EventCode : std::int32_t
        {
            BOARD_CHANGED,
            MOVE
        };

        [[nodiscard]] virtual auto board() -> const std::weak_ptr<core::Board2D>& = 0;
        virtual void               board(std::weak_ptr<core::Board2D>)            = 0;
        [[nodiscard]] virtual auto color_p1() const -> const bwidgets::Color&     = 0;
        virtual void               color_p1(bwidgets::Color)                      = 0;
        [[nodiscard]] virtual auto color_p2() const -> const bwidgets::Color&     = 0;
        virtual void               color_p2(bwidgets::Color)                      = 0;
        [[nodiscard]] virtual auto default_color_p1() const
          -> const bwidgets::Color& = 0;
        [[nodiscard]] virtual auto default_color_p2() const
          -> const bwidgets::Color&                                    = 0;
        [[nodiscard]] virtual auto event_type() const -> std::uint32_t = 0;
    };
}

#endif
