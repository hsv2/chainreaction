#ifndef GUI_SDLUI
#define GUI_SDLUI

#include <memory>
#include <thread>

#include <basic_widgets/core/type/deleter.hpp>

#include "game_screen.hpp"

extern "C" {
    union SDL_Event;
    struct SDL_UserEvent;
    struct SDL_Window;
}

namespace bwidgets
{
    class Renderer;
}

namespace sdlui
{
    class ChainReactSDL final
    {
    public:
        ChainReactSDL();
        ~ChainReactSDL();

        void run();

    private:
        void handle_board_event(const SDL_UserEvent&);
        void handle_event(const SDL_Event&);

        bool                                           _accept_input;
        core::Player                                   _current_player;
        const uint32_t                                 _header_event;
        std::thread                                    _move_thread;
        bool                                           _quit;
        std::unique_ptr<GameScreen>                    _game_screen;
        std::shared_ptr<bwidgets::Renderer>            _renderer;
        std::shared_ptr<bwidgets::Theme>               _theme;
        std::unique_ptr<SDL_Window, bwidgets::Deleter> _window;
    };
}

#endif
