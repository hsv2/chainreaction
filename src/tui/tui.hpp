#include <functional>
#include <optional>

#include "../core/board2d.hpp"
#include "../core/player.hpp"

namespace tui
{
    using namespace core;

    template<typename Lambda>
    concept PlayCB =
      std::is_convertible_v<Lambda, std::function<void(const Board2D::Space&)>>;

    void board_print(const Board2D&);
    auto move_ask() -> std::optional<Board2D::Coord>;

    template<PlayCB Cb>
    auto play_move(Board2D* b, Board2D::Coord c, Player p, Cb cb) -> Player
    {
        auto move_stepper = b->move(c, p);
        while (move_stepper) {
            const auto [modificated_space, winner] = move_stepper();
            cb(modificated_space);
            if (winner) return winner.value();
        }

        return Player::NONE;
    }
}
