#include <charconv>
#include <iostream>
#include <optional>
#include <string>

#include "tui.hpp"

#include <system_error>

using namespace core;
using namespace std;
using namespace tui;

void tui::board_print(const Board2D& b)
{
    auto [w, h] = b.size();
    for (Board2D::size_type y = 0; y < h; y++) {
        for (Board2D::size_type x = 0; x < w; x++) {
            const auto& space = b({x, y});
            cout << "|" << static_cast<int>(space.owner) << ":" << space.value;
        }
        cout << endl;
    }

    for (Board2D::size_type x = 0; x < w; x++) cout << "-";
    cout << endl;
}

auto tui::move_ask() -> optional<Board2D::Coord>
{
    const auto             success_code = std::errc();
    std::from_chars_result result;
    Board2D::size_type     x, y;
    string                 x_input, y_input;

    cout << "Play move:" << endl << "\tx: ";
    cin >> x_input;
    result = std::from_chars(x_input.c_str(), x_input.c_str() + x_input.size(), x);
    if (result.ec != success_code) return nullopt;

    cout << "\ty: ";
    cin >> y_input;
    result = std::from_chars(y_input.c_str(), y_input.c_str() + y_input.size(), y);
    if (result.ec != success_code) return nullopt;

    return make_optional(Board2D::Coord {x, y});
}
