#include <cstdlib>
#include <iostream>

#include "tui.hpp"

using namespace core;
using namespace std;
using namespace tui;

int main()
{
    Board2D b {3, 3};
    Player  p {Player::P1};
    do {
        try {
            while (true) {
                cout << "Player " << static_cast<int>(p) << endl;
                const auto opt = move_ask();
                if (!opt.has_value()) break;

                const auto coord = opt.value();
                if (const auto winner =
                      play_move(&b, coord, p, [&b](auto) { board_print(b); });
                    winner != Player::NONE)
                {
                    cout << "Player " << static_cast<int>(p) << " won" << endl;
                    break;
                }

                cout << endl;
                p = p == Player::P1 ? Player::P2 : Player::P1;
            }
            break;
        } catch (const Board2D::InvalidMove& e) {
            cerr << "Invalid move!" << std::endl;
        }
    } while (true);

    return EXIT_SUCCESS;
}
